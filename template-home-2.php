<?php
/*template name: Home - Recent Posts */
get_header(); ?>

<?php $options = get_option('salient'); ?>

<?php nectar_page_header(get_option('page_for_posts')); ?>

<div id="featured" data-caption-animation="<?php echo (!empty($options['slider-caption-animation']) && $options['slider-caption-animation'] == 1) ? '1' : '0'; ?>" data-bg-color="<?php if(!empty($options['slider-bg-color'])) echo $options['slider-bg-color']; ?>" data-slider-height="<?php if(!empty($options['slider-height'])) echo $options['slider-height']; ?>" data-animation-speed="<?php if(!empty($options['slider-animation-speed'])) echo $options['slider-animation-speed']; ?>" data-advance-speed="<?php if(!empty($options['slider-advance-speed'])) echo $options['slider-advance-speed']; ?>" data-autoplay="<?php echo $options['slider-autoplay'];?>">

	<?php
	$slides = new WP_Query( array( 'post_type' => 'home_slider', 'posts_per_page' => -1, 'order' => 'ASC', 'orderby' => 'menu_order' ) );
	if( $slides->have_posts() ) : ?>

	<?php while( $slides->have_posts() ) : $slides->the_post();

	$alignment = get_post_meta($post->ID, '_nectar_slide_alignment', true);

	$video_embed = get_post_meta($post->ID, '_nectar_video_embed', true);
	$video_m4v = get_post_meta($post->ID, '_nectar_video_m4v', true);
	$video_ogv = get_post_meta($post->ID, '_nectar_video_ogv', true);
	$video_poster = get_post_meta($post->ID, '_nectar_video_poster', true);

	?>

	<div class="slide orbit-slide <?php if( !empty($video_embed) || !empty($video_m4v)) { echo 'has-video'; } else { echo $alignment; } ?>">

		<?php $image = get_post_meta($post->ID, '_nectar_slider_image', true); ?>
		<article data-background-cover="<?php echo (!empty($options['slider-background-cover']) && $options['slider-background-cover'] == 1) ? '1' : '0'; ?>" style="background-image: url('<?php echo $image; ?>')">
			<div class="container">
				<div class="col span_12">
					<div class="post-title">

						<?php
						$wp_version = floatval(get_bloginfo('version'));

									//video embed
						if( !empty( $video_embed ) ) {

							echo '<div class="video">' . do_shortcode($video_embed) . '</div>';

						}
							        //self hosted video pre 3-6
						else if( !empty($video_m4v) && $wp_version < "3.6" || !empty($video_ogv) && $wp_version < "3.6") {

							echo '<div class="video">';
							nectar_video($post->ID);
							echo '</div>';

						}
							        //self hosted video post 3-6
						else if($wp_version >= "3.6"){

							if(!empty($video_m4v) || !empty($video_ogv)) {

								$video_output = '[video ';

								if(!empty($video_m4v)) { $video_output .= 'mp4="'. $video_m4v .'" '; }
								if(!empty($video_ogv)) { $video_output .= 'ogv="'. $video_ogv .'"'; }

								$video_output .= ' poster="'.$video_poster.'"]';

								echo '<div class="video">' . do_shortcode($video_output) . '</div>';
							}
						}

						?>

						<?php
								 //mobile more info button for video
						if( !empty($video_embed) || !empty($video_m4v)) { echo '<div><a href="#" class="more-info"><span class="mi">'.__("More Info",NECTAR_THEME_NAME).'</span><span class="btv">'.__("Back to Video",NECTAR_THEME_NAME).'</span></a></div>'; } ?>

						<?php $caption = get_post_meta($post->ID, '_nectar_slider_caption', true); ?>
						<h2 data-has-caption="<?php echo (!empty($caption)) ? '1' : '0'; ?>"><span>
							<?php echo $caption; ?>
						</span></h2>

						<?php
						$button = get_post_meta($post->ID, '_nectar_slider_button', true);
						$button_url = get_post_meta($post->ID, '_nectar_slider_button_url', true);

						if(!empty($button)) { ?>
						<a href="<?php echo $button_url; ?>" class="uppercase"><?php echo $button; ?></a>
						<?php } ?>


					</div><!--/post-title-->
				</div>
			</div>
		</article>
	</div>
<?php endwhile; ?>
<?php else: ?>


<?php endif; ?>
<?php wp_reset_postdata(); ?>
</div>


<div class="home-wrap">

	<div class="container-wrap">

		<div class="container main-content">

			<?php if(!is_home()) { ?>
			<div class="row">
				<div class="col span_12 section-title blog-title">
					<h1>
						<?php if(is_author()){

							printf( __( 'All posts by %s', NECTAR_THEME_NAME ), get_the_author() );

						} else if(is_category()) {

							printf( __( 'Category Archives: %s', NECTAR_THEME_NAME ), single_cat_title( '', false ) );

						} else if(is_date()){

							if ( is_day() ) :
								printf( __( 'Daily Archives: %s', NECTAR_THEME_NAME ), get_the_date() );

							elseif ( is_month() ) :
								printf( __( 'Monthly Archives: %s', NECTAR_THEME_NAME ), get_the_date( _x( 'F Y', 'monthly archives date format', NECTAR_THEME_NAME ) ) );

							elseif ( is_year() ) :
								printf( __( 'Yearly Archives: %s', NECTAR_THEME_NAME ), get_the_date( _x( 'Y', 'yearly archives date format', NECTAR_THEME_NAME ) ) );

							else :
								_e( 'Archives', NECTAR_THEME_NAME );

							endif;
						} else {
							wp_title("",true);
						} ?>
					</h1>
				</div>
			</div>
			<?php } ?>

			<div class="row">

			<?php //$options = get_option('salient');

			$blog_type = $options['blog_type'];
			if($blog_type == null) $blog_type = 'std-blog-sidebar';

			$masonry_class = null;
			$masonry_style = null;
			$infinite_scroll_class = null;

			//enqueue masonry script if selected
			if($blog_type == 'masonry-blog-sidebar' || $blog_type == 'masonry-blog-fullwidth' || $blog_type == 'masonry-blog-full-screen-width') {
				$masonry_class = 'masonry';
			}

			if($blog_type == 'masonry-blog-full-screen-width') {
				$masonry_class = 'masonry full-width-content';
			}

			if(!empty($options['blog_pagination_type']) && $options['blog_pagination_type'] == 'infinite_scroll'){
				$infinite_scroll_class = ' infinite_scroll';
			}

			if($masonry_class != null) {
				$masonry_style = (!empty($options['blog_masonry_type'])) ? $options['blog_masonry_type']: 'classic';
			}

			if($blog_type == 'std-blog-sidebar' || $blog_type == 'masonry-blog-sidebar'){
				echo '<div id="post-area" class="col span_9 '.$masonry_class.' '.$masonry_style.' '. $infinite_scroll_class.'">';
			} else {
				echo '<div id="post-area" class="col span_12 col_last '.$masonry_class.' '.$masonry_style.' '. $infinite_scroll_class.'">';
			}

			?>

			<div class="youtube-videos-container">
				<?php if (have_posts()) : while (have_posts()) : the_post();?>
					<article id="post-<?php the_ID(); ?>"> 

						<!-- entry content -->
						<div class="entry-content">

							<?php the_content(); ?>

						</div>
						<!-- entry content end--> 
					</article>
					<!--post-end-->

				<?php endwhile; ?>
			<?php endif; ?>
		</div>

		<?php

		echo '<div class="posts-container">';

		$paged = ( get_query_var('page') ) ? get_query_var('page') : 1;
		$args = array(
			'post_type' => array( 'post', 'medium-feed', 'entrepreneur-feed' ),
			'paged' => $paged,
			'posts_per_page' => 12
			);
		$page_query = new WP_Query( $args );

		if($page_query->have_posts()) : while($page_query->have_posts()) : $page_query->the_post(); ?>

		<?php

		if ( floatval(get_bloginfo('version')) < "3.6" ) {
						//old post formats before they got built into the core
			get_template_part( 'includes/post-templates-pre-3-6/entry', get_post_format() );
		} else {
						//WP 3.6+ post formats
			get_template_part( 'includes/post-templates/entry', get_post_format() );
		} ?>

	<?php endwhile; endif; ?>

</div><!--/posts container-->

<?php custom_nectar_pagination( $page_query ); wp_reset_postdata(); ?>

</div><!--/span_9-->

<?php  if($blog_type == 'std-blog-sidebar' || $blog_type == 'masonry-blog-sidebar') { ?>
<div id="sidebar" class="col span_3 col_last">
	<?php get_sidebar(); ?>
</div><!--/span_3-->
<?php } ?>

</div><!--/row-->

<!-- Page content area -->
<div class="row">
	<div class="col span_12 col_last">
		
	</div>
</div>

</div><!--/container-->

</div><!--/container-wrap-->

</div><!--/home-wrap-->

<?php get_footer(); ?>