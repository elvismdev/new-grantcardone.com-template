<?php

add_action( 'wp_enqueue_scripts', 'salient_child_enqueue_styles',50);
function salient_child_enqueue_styles() {
	wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
}

#-----------------------------------------------------------------#
# Pagination
#-----------------------------------------------------------------#

if ( !function_exists( 'custom_nectar_pagination' ) ) {

	function custom_nectar_pagination( $page_query = null ) {

		global $options;
		//extra pagination
		if( !empty($options['extra_pagination']) && $options['extra_pagination'] == '1' ){

			global $wp_query, $wp_rewrite;

			$wp_query->query_vars['paged'] > 1 ? $current = $wp_query->query_vars['paged'] : $current = 1;
			$total_pages = $wp_query->max_num_pages;

			if ($total_pages > 1){

				$permalink_structure = get_option('permalink_structure');
				$query_type = (count($_GET)) ? '&' : '?';
				$format = empty( $permalink_structure ) ? $query_type.'paged=%#%' : 'page/%#%/';

				echo '<div id="pagination" data-is-text="'.__("All items loaded", NECTAR_THEME_NAME).'">';

				echo paginate_links(array(
					'base' => get_pagenum_link(1) . '%_%',
					'format' => $format,
					'current' => $current,
					'total' => $total_pages,
					));

				echo  '</div>';

			}
		}
		//regular pagination
		else{

			if( $page_query->max_num_pages > 1 || ( get_next_posts_link() || get_previous_posts_link() ) ) {
				echo '<div id="pagination" data-is-text="'.__("All items loaded", NECTAR_THEME_NAME).'">
				<div class="prev">'.get_previous_posts_link('&laquo; Previous Entries').'</div>
				<div class="next">'.get_next_posts_link('Load More', ($page_query->max_num_pages) ? $page_query->max_num_pages : '' ).'</div>
			</div>';
		}
	}


}
}

function add_post_class_in_media_page( $classes ) {
	if ( is_page_template( array( 'template-media.php', 'template-medium-feed.php' ) ) 
		|| is_tax( 'source' ) 
		|| is_front_page() ) {
		array_push( $classes, 'post' );
}
return $classes;
}
add_filter( 'post_class', 'add_post_class_in_media_page' );

// Replace default infinitescroll.js
function replace_default_infinitescroll_script() {

	global $post;
	global $options;

	if ( stripos( $post->post_content, '[nectar_blog') !== FALSE || stripos( $portfolio_extra_content, '[nectar_blog') !== FALSE
		|| stripos( $post->post_content, 'pagination_type="infinite_scroll"') !== FALSE || stripos( $portfolio_extra_content, 'pagination_type="infinite_scroll"') !== FALSE
		|| (!empty($options['blog_pagination_type']) && $options['blog_pagination_type'] == 'infinite_scroll')
		|| (!empty($options['portfolio_pagination_type']) && $options['portfolio_pagination_type'] == 'infinite_scroll')
		|| (((is_archive()) || (is_author()) || (is_category()) || (is_home()) || (is_tag())) && ( $posttype == 'post') && (!is_singular())) ) {
		wp_deregister_script( 'infinite_scroll' );
	wp_enqueue_script( 'infinite_scroll', get_stylesheet_directory_uri() . '/js/infinitescroll.js', array('jquery'), '1.0', true );
	wp_enqueue_script( 'infinite_scroll_manual_trigger', get_stylesheet_directory_uri() . '/js/manual-trigger.js', array('jquery'), '1.0', true );
}
}
add_action( 'wp_enqueue_scripts', 'replace_default_infinitescroll_script', 100 );

// Adding Customs JS to footer
function hook_custom_js() { ?>
<!-- Periscope On Air button -->
<script>window.twttr=function(t,e,r){var n,i=t.getElementsByTagName(e)[0],w=window.twttr||{};return t.getElementById(r)?w:(n=t.createElement(e),n.id=r,n.src="https://platform.twitter.com/widgets.js",i.parentNode.insertBefore(n,i),w._e=[],w.ready=function(t){w._e.push(t)},w)}(document,"script","twitter-wjs")</script>
<!-- Twitter profile follow button -->
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
<!-- Youtube and Google Plus Follow button -->
<script src="https://apis.google.com/js/platform.js"></script>
<?php }
add_action( 'wp_footer', 'hook_custom_js' );

// Hard coding Pericope On Air button as a last menu item
function add_periscope_live_btn( $items, $args ) {
	if(isset($args->menu) && $args->menu->term_id == 7){
		$items .= '<li id="menu-item-periscope-live-btn" class="menu-item menu-item-periscope-live-btn"><a href="https://www.periscope.tv/grantcardone" class="periscope-on-air" data-size="large">@GrantCardone</a></li>';
	}
	return $items;
}
add_filter('wp_nav_menu_items','add_periscope_live_btn', 10, 2);

// Add Hook after opening  <body> tag.
function wp_after_body() {
	do_action('wp_after_body');
}

// Add FB SDK for follow button on sidebar
function fbsdkhead() {
	?>
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5";
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>
	<?php
}
add_action( 'wp_after_body', 'fbsdkhead' );

// Make pemalinks of RSS feeds imported link to it's original source link instead the default WordPress
add_filter( 'wprss_ftp_link_post_title', '__return_true' );


// ALLOW FONTAWESOME ICONS ON WIDGET TITLES
function awesome_widget_title( $title ) {
	$title = str_replace( '[fa-icon=chevron-right]', '<i class="fa fa-chevron-right fa-lg"></i>', $title );
	return $title;
}
add_filter( 'widget_title', 'awesome_widget_title' );


// Remove WooCommerce scripts from non WooCommerce pages
/**
 * Optimize WooCommerce Scripts
 * Remove WooCommerce Generator tag, styles, and scripts from non WooCommerce pages.
 */
function child_manage_woocommerce_styles() {
	//remove generator meta tag
	remove_action( 'wp_head', array( $GLOBALS['woocommerce'], 'generator' ) );

	//first check that woo exists to prevent fatal errors
	if ( function_exists( 'is_woocommerce' ) ) {
		//dequeue scripts and styles
		if ( ! is_woocommerce() && ! is_cart() && ! is_checkout() ) {
			wp_dequeue_style( 'woocommerce-layout' );
			wp_dequeue_style( 'woocommerce-smallscreen' );
			wp_dequeue_style( 'woocommerce-general' );
			wp_dequeue_style( 'woocommerce' );
			wp_dequeue_script( 'wc-cart-fragments' );
			wp_dequeue_script( 'woocommerce' );
			// wp_dequeue_script( 'wc-add-to-cart' );
			// wp_dequeue_script( 'jquery-blockui' );
			// wp_dequeue_script( 'jqueryui' );
			// wp_dequeue_style( 'amazon_payments_advanced' );
			// wp_dequeue_style( 'wc-bundle-style' );
			// wp_dequeue_script( 'wc_price_slider' );
			// wp_dequeue_script( 'wc-single-product' );
			// wp_dequeue_script( 'wc-checkout' );
			// wp_dequeue_script( 'wc-add-to-cart-variation' );
			// wp_dequeue_script( 'wc-single-product' );
			// wp_dequeue_script( 'wc-cart' );
			// wp_dequeue_script( 'wc-chosen' );
			// wp_dequeue_script( 'prettyPhoto' );
			// wp_dequeue_script( 'prettyPhoto-init' );
			// wp_dequeue_script( 'jquery-placeholder' );
			// wp_dequeue_script( 'fancybox' );
			// wp_dequeue_script( 'amazon_payments_advanced' );
			// wp_dequeue_script( 'amazon_payments_advanced_widgets' );
		}
	}

}
add_action( 'wp_enqueue_scripts', 'child_manage_woocommerce_styles', 100 );
// END Remove WooCommerce scripts from non WooCommerce pages


// Remove isotope.pkgd.min.js file added by the YouTube Vusual Composer addon. It was giving JS conflitcs but the plugin still works without it.
function gc_remove_vc_yt_isotope() {
	wp_deregister_script( 'sb-isotope' );
}
add_action( 'wp_enqueue_scripts', 'gc_remove_vc_yt_isotope', 100 );

// Adds any extra assets like additional libraries to help enhance functionality on the template.
function gc_add_extra_assets() {
	// Tooltipster
	wp_enqueue_style( 'tooltipster', get_stylesheet_directory_uri() . '/assets/tooltipster/css/tooltipster.css', array(), '3.3.0' );
	wp_enqueue_script( 'tooltipster', get_stylesheet_directory_uri() . '/assets/tooltipster/js/jquery.tooltipster.min.js', array('jquery'), '3.3.0', true );
}
add_action( 'wp_enqueue_scripts', 'gc_add_extra_assets', 100 );

// Tooltips for the podcasts images on mouse over.
function gc_podcasts_html_tooltips() { ?>
<script>
	jQuery(document).ready(function() {

		// Cardone Zone
		jQuery('#cardone-zone').tooltipster({
			content: jQuery('<span class="hover">\
				<ul>\
					<?php if ( is_page( 35180 ) ): ?><li class="podcast-title">Podcast</li><?php endif; ?>
					<li><a class="itunes-link" target="_blank" href="https://itunes.apple.com/us/podcast/cardone-zone/id825614458"><img class="itunes-icon" src="/wp-content/uploads/2015/10/itunes.png" /> iTunes</a></li>\
					<li><a class="stitcher-link" target="_blank" href="http://www.stitcher.com/podcast/cardone-zone"><img class="stitcher-icon" src="/wp-content/uploads/2015/10/stitcher.png" /> Stitcher</a></li>\
				</ul>\
			</span>'),
			contentAsHTML: true,
			interactive: true,
			theme: 'tooltipster-podcasts',
			<?php if ( is_page( 35180 ) ): ?>position: 'bottom'<?php endif; ?>
		});

		// The G&E Show
		jQuery('#ge-show').tooltipster({
			content: jQuery('<span class="hover">\
				<ul>\
					<?php if ( is_page( 35180 ) ): ?><li class="podcast-title">Podcast</li><?php endif; ?>
					<li><a class="itunes-link" target="_blank" href="https://itunes.apple.com/us/podcast/the-g-e-show/id889222422"><img class="itunes-icon" src="/wp-content/uploads/2015/10/itunes.png" /> iTunes</a></li>\
					<li><a class="stitcher-link" target="_blank" href="http://www.stitcher.com/podcast/the-ge-show"><img class="stitcher-icon" src="/wp-content/uploads/2015/10/stitcher.png" /> Stitcher</a></li>\
				</ul>\
			</span>'),
			contentAsHTML: true,
			interactive: true,
			theme: 'tooltipster-podcasts',
			<?php if ( is_page( 35180 ) ): ?>position: 'bottom'<?php endif; ?>
		});

		// Power Players
		jQuery('#power-players').tooltipster({
			content: jQuery('<span class="hover">\
				<ul>\
					<?php if ( is_page( 35180 ) ): ?><li class="podcast-title">Podcast</li><?php endif; ?>
					<li><a class="itunes-link" target="_blank" href="https://itunes.apple.com/us/podcast/power-players/id966353280"><img class="itunes-icon" src="/wp-content/uploads/2015/10/itunes.png" /> iTunes</a></li>\
					<li><a class="stitcher-link" target="_blank" href="http://www.stitcher.com/podcast/power-players"><img class="stitcher-icon" src="/wp-content/uploads/2015/10/stitcher.png" /> Stitcher</a></li>\
				</ul>\
			</span>'),
			contentAsHTML: true,
			interactive: true,
			theme: 'tooltipster-podcasts',
			<?php if ( is_page( 35180 ) ): ?>position: 'bottom'<?php endif; ?>
		});

		// Young Hustlers
		jQuery('#young-hustlers').tooltipster({
			content: jQuery('<span class="hover">\
				<ul>\
					<?php if ( is_page( 35180 ) ): ?><li class="podcast-title">Podcast</li><?php endif; ?>
					<li><a class="itunes-link" target="_blank" href="https://itunes.apple.com/us/podcast/young-hustlers/id860180416"><img class="itunes-icon" src="/wp-content/uploads/2015/10/itunes.png" /> iTunes</a></li>\
					<li><a class="stitcher-link" target="_blank" href="http://www.stitcher.com/podcast/young-hustlers"><img class="stitcher-icon" src="/wp-content/uploads/2015/10/stitcher.png" /> Stitcher</a></li>\
				</ul>\
			</span>'),
			contentAsHTML: true,
			interactive: true,
			theme: 'tooltipster-podcasts',
			<?php if ( is_page( 35180 ) ): ?>position: 'bottom'<?php endif; ?>
		});

		// Confessions of an Entrepreneur
		jQuery('#confessions-entrepreneur').tooltipster({
			content: jQuery('<span class="hover">\
				<ul>\
					<?php if ( is_page( 35180 ) ): ?><li class="podcast-title">Podcast</li><?php endif; ?>
					<li><a class="itunes-link" target="_blank" href="https://itunes.apple.com/us/podcast/confessions-entrepreneur/id964629848"><img class="itunes-icon" src="/wp-content/uploads/2015/10/itunes.png" /> iTunes</a></li>\
					<li><a class="stitcher-link" target="_blank" href="http://www.stitcher.com/podcast/confessions-of-an-entrepreneur"><img class="stitcher-icon" src="/wp-content/uploads/2015/10/stitcher.png" /> Stitcher</a></li>\
				</ul>\
			</span>'),
			contentAsHTML: true,
			interactive: true,
			theme: 'tooltipster-podcasts',
			<?php if ( is_page( 35180 ) ): ?>position: 'bottom'<?php endif; ?>
		});

	});
</script>
<?php }
add_action( 'wp_footer', 'gc_podcasts_html_tooltips' );

// Filter the output of the author link just to go to the homepage.
function gc_replace_author_link_to_homepage( $link ) {

	if ( stripos( $link, 'author/grant/' ) === false ) return $link;

	$link = str_replace( 'author/grant/', '', $link );
	return $link;

}
add_filter( 'the_author_posts_link', 'gc_replace_author_link_to_homepage' );


?>